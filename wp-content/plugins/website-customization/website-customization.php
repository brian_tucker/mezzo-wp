<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://gatorworks.net
 * @since             1.0.0
 * @package           GW_Website_Customization
 *
 * @wordpress-plugin
 * Plugin Name:       Website Customization
 * Plugin URI:        https://gatorworks.net
 * Description:       Creates the custom content and structures for the Affordable Smiles Website, including custom post types and custon Gutenberg blocks.. Deactivating this plugin will break components of the site.
 * Version:           1.0.0
 * Author:            Gatorworks
 * Author URI:        https://gatorworks.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gw-website-customization
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );


add_action('acf/init', 'my_register_blocks');
function my_register_blocks() {

    // check function exists
    if( function_exists('acf_register_block') ) {

        // register a CTA block
        acf_register_block(array(
            'name'              => 'cta',
            'title'             => __('CTA'),
            'description'       => __('A custom call-to-action block.'),
            'render_template'   => 'template-parts/blocks/cta.php',
            'category'          => 'common',
            'icon'              => 'megaphone',
            'mode'              => 'auto',
            'keywords'          => array( 'cta', 'callout', 'button', 'call', 'action' ),
        ));

        // register an accordion block
        acf_register_block(array(
            'name'              => 'accordion',
            'title'             => __('Accordion'),
            'description'       => __('A custom accordion block.'),
            'render_template'   => 'template-parts/blocks/accordion.php',
            'category'          => 'common',
            'icon'              => 'editor-justify',
            'mode'              => 'edit',
            'keywords'          => array( 'accordion', 'hide', 'reveal', 'faq' ),
        ));

        // register an accordion block
        acf_register_block(array(
            'name'              => 'testimonial',
            'title'             => __('Testimonial'),
            'description'       => __('A custom testimonial block.'),
            'render_template'   => 'template-parts/blocks/testimonial.php',
            'category'          => 'common',
            'icon'              => 'microphone',
            'mode'              => 'edit',
            'keywords'          => array( 'testimonial', 'quote' ),
        ));

        // register an image link block
        acf_register_block(array(
            'name'              => 'image-link-grid',
            'title'             => __('Styled Image Link Grid'),
            'description'       => __('A grid containing image link blocks with a title.'),
            'render_template'   => 'template-parts/blocks/image-link-grid.php',
            'category'          => 'common',
            'icon'              => 'screenoptions',
            'mode'              => 'edit',
            'keywords'          => array( 'grid', 'image', 'link' ),
        ));


        // register an hero image block
        acf_register_block(array(
            'name'              => 'hero',
            'title'             => __('Hero'),
            'description'       => __('A styled image block with content'),
            'render_template'   => 'template-parts/blocks/hero.php',
            'category'          => 'common',
            'icon'              => 'format-image',
            'mode'              => 'edit',
            'keywords'          => array( 'hero', 'image', 'banner' ),
        ));

        //registering the pi-card block
        acf_register_block(array(
            'name'              => 'pi-card',
            'title'             => __('Products & Industries Card'),
            'description'       => __('A styled card with images and text'),
            'render_template'   => 'template-parts/blocks/pi-card.php',
            'category'          => 'common',
            'icon'              => 'images-alt',
            'mode'              => 'edit',
            'keywords'          => array( 'card', 'image', 'text' ),
        ));

        //registering the employee-card block
        acf_register_block(array(
            'name'              => 'employee-card',
            'title'             => __('Employee Card'),
            'description'       => __('A styled card with images and text'),
            'render_template'   => 'template-parts/blocks/employee-card.php',
            'category'          => 'common',
            'icon'              => 'admin-users',
            'mode'              => 'edit',
            'keywords'          => array( 'card', 'image', 'text' ),
        ));

        //registering the employee-card block
        acf_register_block(array(
            'name'              => 'case-study',
            'title'             => __('Case Study'),
            'description'       => __('A block used by the product children'),
            'render_template'   => 'template-parts/blocks/case-study.php',
            'category'          => 'common',
            'icon'              => 'portfolio',
            'mode'              => 'edit',
            'keywords'          => array( 'case', 'study', 'text' ),
        ));

        //registering the slider gallery block
        acf_register_block(array(
            'name'              => 'slider-gallery',
            'title'             => __('Slider Gallery'),
            'description'       => __('A block used by the product children'),
            'render_template'   => 'template-parts/blocks/slider-gallery.php',
            'category'          => 'common',
            'icon'              => 'images-alt2',
            'mode'              => 'edit',
            'keywords'          => array( 'case', 'study', 'text' ),
        ));
    }
}


// Register Custom Post Types

// Register Custom Post Type
function products_post_type() {

    $labels = array(
        'name'                  => _x( 'Products', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Products', 'text_domain' ),
        'name_admin_bar'        => __( 'Products', 'text_domain' ),
        'archives'              => __( 'Products Archives', 'text_domain' ),
        'attributes'            => __( 'Products Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Product:', 'text_domain' ),
        'all_items'             => __( 'All Products', 'text_domain' ),
        'add_new_item'          => __( 'Add New Product', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'view_items'            => __( 'View Items', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Product', 'text_domain' ),
        'description'           => __( 'Archive of all products', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-products',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true,
    );
    register_post_type( 'products', $args );

}
add_action( 'init', 'products_post_type', 0 );


function industry_post_type() {
    $labels = array(
        'name'                  => _x( 'Industries', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Industry', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Industries', 'text_domain' ),
        'name_admin_bar'        => __( 'Industries', 'text_domain' ),
        'archives'              => __( 'Industries Archives', 'text_domain' ),
        'attributes'            => __( 'Industries Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Product:', 'text_domain' ),
        'all_items'             => __( 'All Industries', 'text_domain' ),
        'add_new_item'          => __( 'Add New Product', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'view_items'            => __( 'View Items', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Product', 'text_domain' ),
        'description'           => __( 'Archive of all industries', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-building',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => true,
    );
    register_post_type( 'industries', $args );
}
add_action( 'init', 'industry_post_type', 0 );

register_activation_hook(__FILE__, 'gw_activation');
function gw_activation() {
    // trigger our function that registers the custom post type
    gw_setup_custom_post_type();
 
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}

register_deactivation_hook(__FILE__, 'gw_deactivation');
function gw_deactivation() {
    // unregister the post type, so the rules are no longer in memory
    unregister_post_type( 'project' );

    // clear the permalinks to remove our post type's rules from the database
    flush_rewrite_rules();
}


// Build out admin columns for pages
add_filter( 'manage_page_posts_columns', 'gw_filter_page_columns' );
function gw_filter_page_columns( $columns ) {
    $columns = array(
        'cb' => $columns['cb'],
        'title' => __( 'Title' ),
        'template' => __( 'Template' ),
        'author' => __( 'Author' ),
        'date' => __( 'Date' ),
    );
  return $columns;
}

add_action( 'manage_page_posts_custom_column', 'gw_page_columns', 10, 2);
function gw_page_columns( $column, $post_id ) {
    // Image column
    if ( 'template' === $column ) {
        $template = get_page_template_slug($post_id);
        if (strpos($template, 'landing')) {
            echo 'Landing Page';
        } else if (strpos($template, 'front')) {
            echo 'Front';
        } else {
            echo 'Default';
        }
    }
}

add_filter( 'manage_edit-page_sortable_columns', 'gw_page_sortable_columns');
function gw_page_sortable_columns( $columns ) {
  $columns['template'] = '_wp_page_template';
  return $columns;
}

add_action( 'pre_get_posts', 'gw_posts_orderby' );
function gw_posts_orderby( $query ) {
  if( ! is_admin() || ! $query->is_main_query() ) {
    return;
  }

  if ( '_wp_page_template' === $query->get( 'orderby') ) {
    $query->set('meta_key','_wp_page_template');
    $query->set('orderby','meta_value');
    // Workaround to include items without this meta key
    // Based on: https://core.trac.wordpress.org/ticket/19653#comment:11
    add_filter('get_meta_sql', function($clauses) {
      $clauses['join'] = str_replace('INNER JOIN','LEFT JOIN',$clauses['join']) . $clauses['where'];
      $clauses['where'] = '';
      return $clauses;
    });
  }
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_sub_page(array(
        'page_title'    => 'Site Options',
        'menu_title'    => 'Site Options',
        'menu_slug'     => 'site-options',
        'capability'    => 'edit_posts',
        'parent_slug'   => 'options-general.php',
        'redirect'      => false,
        // 'icon_url' => 'dashicons-art'
    ));
}


// Move Yoast to bottom
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

// Restrict ACF privileges to admins
function my_acf_settings_capability( $path ) {
  return 'administrator';
}
add_filter('acf/settings/capability', 'my_acf_settings_capability');



/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com
 */
 
/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;
 
    if ( is_search() ) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
 
    return $join;
}
add_filter('posts_join', 'cf_search_join' );
 
/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;
 
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }
 
    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );
 
/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;
 
    if ( is_search() ) {
        return "DISTINCT";
    }
 
    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );
