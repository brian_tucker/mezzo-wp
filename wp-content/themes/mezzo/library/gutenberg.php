<?php

if ( ! function_exists( 'foundationpress_gutenberg_support' ) ) :
	function foundationpress_gutenberg_support() {

    // Add foundation color palette to the editor
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'Primary Color', 'foundationpress' ),
            'slug' => 'primary',
            'color' => '#1779ba',
        ),
        array(
            'name' => __( 'Secondary Color', 'foundationpress' ),
            'slug' => 'secondary',
            'color' => '#767676',
        ),
        array(
            'name' => __( 'Success Color', 'foundationpress' ),
            'slug' => 'success',
            'color' => '#3adb76',
        ),
        array(
            'name' => __( 'Warning color', 'foundationpress' ),
            'slug' => 'warning',
            'color' => '#ffae00',
        ),
        array(
            'name' => __( 'Alert color', 'foundationpress' ),
            'slug' => 'alert',
            'color' => '#cc4b37',
        )
    ) );

	}

	add_action( 'after_setup_theme', 'foundationpress_gutenberg_support' );
endif;

function digwp_disable_gutenberg($is_enabled, $post_type) {
    //if ($post_type === 'urgent-care-centers') return false; 
    // if ($post_type === 'post') return false; 
    return $is_enabled;
}
add_filter('use_block_editor_for_post_type', 'digwp_disable_gutenberg', 10, 2);
/**
 * Templates and Page IDs without editor
 *
 */
function ea_disable_editor( $id = false ) {
    $excluded_templates = array(
        'page-templates/front.php',
        //'page-templates/page-venue-archives.php',
    );
    $excluded_ids = array(
        // get_option( 'page_on_front' )
    );
    if( empty( $id ) )
        return false;
    $id = intval( $id );
    $template = get_page_template_slug( $id );
    return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates ) ;
}
/**
 * Disable Gutenberg by template
 *
 */
function ea_disable_gutenberg( $can_edit, $post_type ) {
    if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
        return $can_edit;
    if( ea_disable_editor( $_GET['post'] ) )
        $can_edit = false;
    return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );
