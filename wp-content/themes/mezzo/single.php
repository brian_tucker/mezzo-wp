<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>


<!-- gets the featured image for the header -->

<div class="feat-cont">
	<div class="news-overlay"></div>
	<?php get_template_part( 'template-parts/featured-image' ); ?>
</div> 
<div class="main-container news-single-main-cont">
	<div class="main-grid">
		<main class="main-content">
			<?php while ( have_posts() ) : the_post(); ?>
				<!-- grabbing the title -->
				<div id="news-cat"><?php the_category(); ?></div>
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
				<?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
			<?php endwhile; ?>
		</main>
	</div>
</div>
<?php get_footer();
