<?php
/*
Template Name: Front
*/
get_header(); ?>


<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>


<!-- This is the parent container which will contain all the elements for this page -->
<div class="front-page">
	<!-- This is the hero section	 -->
	<section class="hero-sect" role="main">
		<div class="fp-intro">
			<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
				<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
				<!-- creating hero var that holds content data -->
				<?php $hero = get_field('hero_area'); ?>
				<div class="mezzo-hero-text">
					<div class="header-group">
						<h1 class="hero-h1">
							<!-- grabbing the h1 from the hero area directly -->
							<?php echo $hero['header1']; ?>
						</h1>
						<h1 class="hero-h1">
							<?php echo $hero['header2']?>
						</h1>
					</div>

					<!-- The anchor for the subheader and subheader line. So that its all clickable -->
					<a id="arrow-cont" href=<?php echo $hero['link']['url']?>>
						<div id="hero-arrow">
							<p>
								<!-- grabbing the subheader from the var we created above -->
								<?php echo $hero['subheader']; ?>
							</p>
							<div id="hero-arrow-cont">
								<hr>
								<div class="arrow-right"></div>
							</div>							
						</div>
					</a>
				</div>
				
				<!-- The images for the hero section -->
				<img class="responsive" id="mezzo-gradient-img" src="/wp-content/themes/mezzo/dist/assets/images/front-page/hero/gradient.png">
				<img class="responsive" id="mezzo-rectangle-img" src="/wp-content/themes/mezzo/dist/assets/images/front-page/hero/Rectangle.png">
				<img class="responsive" id="mezzo-tube-img" src="/wp-content/themes/mezzo/dist/assets/images/front-page/hero/microtubes.png">

				<div id="hero-mobile-imgs">
					<!-- Mobile Imgs -->
					<img class="responsive" id="mezzo-rectangle-mobile" src="/wp-content/themes/mezzo/dist/assets/images/mobile/Rectangle.png">
					<img class="responsive" id="mezzo-gradient-mobile" src="/wp-content/themes/mezzo/dist/assets/images/front-page/hero/gradient.png">
					<img class="responsive" id="mezzo-tube-mobile" src="/wp-content/themes/mezzo/dist/assets/images/mobile/microtubes.png">
				</div>
				

				<?php do_action( 'foundationpress_page_before_comments' ); ?>
				<?php comments_template(); ?>
				<?php do_action( 'foundationpress_page_after_comments' ); ?>
			</div>

		</div>
	</section>
	<?php endwhile; ?>
	<?php do_action( 'foundationpress_after_content' ); ?>

	<div class="sect-div">
		<!-- <hr /> -->
	</div>


	<!-- This is the intro section -->
	<section class="intro-sect">
		<div class="intro-div">
			<header>

				<div id="intro-mobile-imgs">
					<img id="mezzo-shell-mobile" src="/wp-content/themes/mezzo/dist/assets/images/mobile/shell.png">
				</div>


				<img class="responsive" id="mezzo-shell-img" src="/wp-content/themes/mezzo/dist/assets/images/front-page/intro/shell-and-tube_3.png">
				<div class="mezzo-intro-text">
					<!-- Defining the intro var to use the data from wp_admin -->
					<?php $intro = get_field('intro_section'); ?>

					<h2>
						<!-- grabbing the h1 from the hero area directly -->
						<?php echo $intro['h2']; ?>
					</h2>
					<p>
						<!-- grabbing the subheader from the var we created above -->
						<?php echo $intro['h3']; ?>
					</p>

					<p id="para-cust">
						<?php echo $intro['para']?>
					</p> 
				
					<!-- <button>CONTACT MEZZO</button> -->
					<a href=<?php echo $intro['button_link']['url'] ?>><button><?php echo strtoupper($intro['button_text']) ?></button></a>

				</div>		
			</header>
		</div>
	</section>

	<!-- <div class="sect-div">
		<hr />
	</div> -->

	<div id="pi-sect-cont">
		<!-- This is the products and industries section -->
		<div id="overlay"></div>
		<img class="responsive"id="mezzo-racetrack-ext" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/racetrack.png">
		<img class="responsive" id="mezzo-rect2-ext" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/Rectangle 2.png">
		<img class="responsive" id="mezzo-rect3-ext" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/Rectangle Copy 3.png">
		<img class="responsive" id="mezzo-mask-ext" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/Mask.png">
		<section class="pi-sect">
			<?php $pi = get_field('products_industries'); ?>
			<div id="mezzo-pi-text">
				<h2 id="title1">
					<?php echo $pi['header1'] ?>
				</h2>
				<h2 id="title2">
					<?php echo $pi['header2']?>
				</h2> 
				<h3 id="head1">
					<?php echo $pi['header1'] ?>
				</h3>
				<h3 id="head2" >
					<?php echo $pi['header2']?>
				</h3>
				<p id="p1">
					<?php echo $pi['p1'] ?>	
				</p>
				<p id="p2">
					<?php echo $pi['p2'] ?>
				</p> 
				<a href=<?php echo $pi['button_link_1']['url']; ?>><button id="b1">
					<?php echo $pi['button_1']?>
				</button></a>
				<a href=<?php echo $pi['button_link_2']['url'];  ?>><button id="b2">
					<?php echo $pi['button_2'] ?>
				</button></a>
			</div>
			<div id="mezzo-pi-mobile-text">
				<div id="mobile-text1">
					<h2 id="mobile-title1">
						<?php echo $pi['mobile_header'] ?>
					</h2>
					<p id="mobile-p1">
						<?php echo $pi['p1'] ?>	
					</p>
					<a href=<?php echo $pi['button_link_1']['url']; ?>><button id="mobile-b1">
						<?php echo $pi['button_1']?>
					</button></a>
				</div>
				<div id="mobile-text2">
					<h2 id="mobile-title2">
					<?php echo $pi['header2']?>
					</h2>
					<p id="mobile-p2">
						<?php echo $pi['p2'] ?>
					</p>
					<a href=<?php echo $pi['button_link_2']['url'];  ?>><button id="mobile-b2">
						<?php echo $pi['button_2'] ?>
					</button></a>
				</div>
			</div>
			<div id="pi-mobile-imgs">
				<img id="mobile-car" src="/wp-content/themes/mezzo/dist/assets/images/mobile/pi/car.png">
				<img id="mobile-oil" src="/wp-content/themes/mezzo/dist/assets/images/mobile/pi/oil.png">
				<img id="mobile-track" src="/wp-content/themes/mezzo/dist/assets/images/mobile/pi/racetrack.png">
				<img id="mobile-rect" src="/wp-content/themes/mezzo/dist/assets/images/mobile/pi/Rectangle.png">
				<img id="mobile-rect2" src="/wp-content/themes/mezzo/dist/assets/images/mobile/pi/Rectangle2.png">
			</div>
			<img class="responsive" id="mezzo-car-img" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/indy-car2.png">
			<img class="responsive" id="mezzo-car-img2" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/indy-car2.png">
			<img class="responsive" id="mezzo-oil-img" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/oil-cooler_full.png">
			<img class="responsive" id="mezzo-oil-img2" src="/wp-content/themes/mezzo/dist/assets/images/front-page/pi/oil-cooler_full.png">
		</section>	
	</div>
</div>


<?php get_footer();
