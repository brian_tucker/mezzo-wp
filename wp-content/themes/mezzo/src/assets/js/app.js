import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

$(document).foundation();


// this is getting the current year for the footer
$('#foot-year').html(new Date().getFullYear());


//Call to the custom functions
ArrowShift();
PIHandler();
EmpExpand();




//when the document is loaded, run this function
$(document).ready(function(){
	//commented out this function...cause not sure if its needed anymore. If not, then I'll just remove it entirely soon.
	PushCTAImg('main-img');
	// NavMenuHandler();
	// console.log("WHHHHHY");
});




//function that will handle the hovering displaying and hiding for the mega nav
// function NavMenuHandler(){
	
// 	//products menu item
// 	var menuItem = document.getElementById('menu-item-49');
// 	//industries menu item.
// 	var menuItem2 = document.getElementById('menu-item-47');


// 	//products tab hover
// 	$(menuItem).hover(function() {
// 		// displaying the info container that'll show the info for the children items
// 		$('#header-info-cont').css('visibility', 'visible');

// 	}, function(){
// 		//hiding the info container for the children when the tab isn't hovered
// 		$('#header-info-cont').css('visibility', 'hidden');
// 	});


// 	//industries tab hover
// 	$(menuItem2).hover(function() {
// 		// displaying the info container that'll show the info for the children items
// 		$('#header-info-cont').css('visibility', 'visible');
// 	}, function(){
// 		//hiding the info container for the children when the tab isn't hovered
// 		$('#header-info-cont').css('visibility', 'hidden');
// 	});

// 	//hovering over the elements to display the child data The data doesn't show up every time, how could i fix that?
// 	$('.menu-item').hover(function() {

// 		var menu_item_id = this.id;

// 		var splitArr;
// 		var card_id, card_pi_img_id, card_ind_img_id, card_text_id;

// 		splitArr = menu_item_id.split('-');
// 		card_id = splitArr[2];
// 		card_id = "#c-" + card_id;
// 		card_pi_img_id = "#c-pi-img-" + splitArr[2];
// 		card_ind_img_id = "#c-ind-img-" + splitArr[2];
// 		card_text_id = "#c-text-" + splitArr[2];

		
// 		//not watching the products or industries items
// 		if(menu_item_id != "menu-item-49" && menu_item_id != "menu-item-47")
// 		{


// 			//the object of the parent for the child
// 			var obj1 = $('#'+menu_item_id).parent();
// 			//picking out just the parent element
// 			var parent_of_item = obj1[0].parentNode;

// 			var parent_of_item_id = $(parent_of_item).attr('id');

// 			console.log(parent_of_item);
// 			console.log("Parent ID: " + parent_of_item_id);


// 			//this is what is displaying the content...how can I modify to fix?
// 			$('#'+menu_item_id).hover(function(){
// 				$(card_pi_img_id).css('display', 'flex');
// 				$(card_pi_img_id).css('z-index', '200');
// 				$(card_ind_img_id).css('display', 'flex');
// 				$(card_ind_img_id).css('z-index', '200');
// 				$(card_text_id).css('display', 'block');
// 				$(card_text_id).css('z-index', '200');

// 			}, function(){
// 				$(card_pi_img_id).css('display', 'none');
// 				$(card_pi_img_id).css('z-index', '0');
// 				$(card_ind_img_id).css('display', 'none');
// 				$(card_ind_img_id).css('z-index', '0');
// 				$(card_text_id).css('display', 'none');
// 				$(card_text_id).css('z-index', '0');
// 			});
// 		}
// 	});
// }




//getting the height of a function passed
var elmHeight = CalcHeight('#mezzo-title-bar');

//calling the function that will shift the element based off of the 2nd & 3rd params, passing it the params
ShiftElement('#offCanvas', elmHeight, 'top');

//this is the function that will figure out 
function PushCTAImg(img_id)
{
	var element = document.getElementById(img_id);

	if(document.getElementsByClassName('entry-title')[0] != null)
	{
		var innerText = document.getElementsByClassName('entry-title')[0].innerText;
		//checking to see if its the why page, cause it has the plane image which sits higher than the rest.
		if(innerText != "Why Microtubes?")
		{
			// $(element).css('display', 'none');
		}
	}
	else //when the entry title doesn't exist. 
	{
		//if the title of the entry isn't why title, then push the main-img on the cta down a bit. This is for the car and military vehicle
		if(innerText != "Why Microtubes?")
		{
			// $(element).css('top','9rem');
		}
	}
}

//with the knowledge I have now, I can probably acheive this using just css............
//This is the function that will handle the expansion of the employee card for mobile styling...
function EmpExpand(){

	//defining the browswer width
	var width = window.innerWidth;

	//if the width is below large...then try to display the text that isn't showing...what are the ids for this?
	if(width < 1050)
	{
		$('.employee-card-cont').click(function(){

			//THIS WORKS
			$(this).toggleClass('expanded');
			$(this).find('.emp-desc').toggleClass('show-text');
			$(this).find('.circle-cont').toggleClass('hide');
			$(this).find('.circle-cont2').toggleClass('show');
		});
	}

	//changing the class to change the circle when the container is clicked. This is for the accordion.
	$('.main_a').click(function(){

		$(this).find('#circle').toggleClass('circle');
		$(this).find('#circle').toggleClass('circle-change');

	});
}

// this is the function that will handle the shift of the arrow in the hero section
function ArrowShift()
{
	$('#arrow-cont').hover( function(){
		//hover over

		$('#hero-arrow-cont').css('right', '-40px');
		//$('top-bar-right')
	}, function() {
		//hover off
		$('#hero-arrow-cont').css('right', '-17px');
	});
}


// this is the function that handles the Products and Industries section
function PIHandler()
{


	//LEFT vars
	var imgOverlay = document.getElementById('mezzo-rect2-img');
	var imgOverlayb = document.getElementById('mezzo-rect2-ext');
	//the original car image to dissapear on hover
	var car = document.getElementById('mezzo-car-img');
	//the 2nd car image to show up on hove 
	var car2 = document.getElementById('mezzo-car-img2');
	//the header 'your industry' to disappear on hover
	var h1 = document.getElementById('head1');
	var t1 = document.getElementById('title1');


	//RIGHT vars
	var imgOverlay2 = document.getElementById('mezzo-rect3-img');
	var imgOverlay2b = document.getElementById('mezzo-rect3-ext');
	var oil = document.getElementById('mezzo-oil-img');
	var oil2 = document.getElementById('mezzo-oil-img2');
	var h2 = document.getElementById('head2');
	var t2 = document.getElementById('title2');



	//LEFT animation
	//This is what shows and hides the elements as they are hovered over for the left side
	$('#mezzo-rect2-ext, #mezzo-car-img2, #title1, #head1, #p1, #b1').hover( 
		function() {
			//here
			$(imgOverlay).css('opacity','0');
			$(imgOverlayb).css('opacity','0');
			//do car stuff here
			$(car).css('opacity', '0');
			$(h1).css('opacity', '0');


			//showing the car and title
			$(car2).css('opacity','1');
			$(t1).css('opacity', '1');

			//lowering the zindex of the header to put behind overlay
			$('#head2').css('z-index', '19');
			$('#head2').css('color', '#164F6B');


		}, function() {
			$(imgOverlay).css('opacity', '1');
			$(imgOverlayb).css('opacity', '1');
			//do car stuff here
			$(car).css('opacity','1');
			$(h1).css('opacity', '1');

			//hiding the car and title
			$(car2).css('opacity','0');	
			$(t1).css('opacity', '0');

			//increasing the z-index of the header to put over overlay
			$('#head2').css('z-index', '60');
			$('#head2').css('color', '#fff');
			
	});

	//RIGHT animiation
	$('#mezzo-rect3-ext, #mezzo-oil-img2, #title2, #head2, #p2, #b2').hover( 
		function() {
			//do car stuff here
			//here
			$(imgOverlay2).css('opacity','0');
			$(imgOverlay2b).css('opacity','0');
			$(oil).css('opacity', '0');
			$(h2).css('opacity', '0');


			//showing the car and title
			$(oil2).css('opacity','1');
			$(t2).css('opacity', '1');

			//lowering the zindex of the header to put behind overlay
			$('#head1').css('z-index', '19');
			$('#head1').css('color', '#164F6B');




		}, function() {
			$(imgOverlay2).css('opacity', '1');
			$(imgOverlay2b).css('opacity','1');
			//do car stuff here
			$(oil).css('opacity','1');
			$(h2).css('opacity', '1');

			//hiding the car and title
			$(oil2).css('opacity','0');	
			$(t2).css('opacity', '0');

			//increasing the z-index of the header to put over overlay
			$('#head1').css('z-index', '60');
			$('#head1').css('color', '#fff');
			
	});
}

//this shifts an element based off of the height of the title bar, since it'll vary. Allows it to fit properly
function ShiftElement(elmToMove, distance, direction)
{
	$(elmToMove).css('margin-' + direction , distance);
}

// function that calculates the height of an element
function CalcHeight(id)
{
	var height = $(id).height();

	return height;
}