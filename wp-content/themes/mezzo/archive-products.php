<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="main-container main-cust">
	
	<!-- maybe put the header and paragraph here? Its repeating in the loop below, which i don't want -->
	<!-- I can maybe have the products field with the data there, get the field and populate here -->
	<!-- I'll have to create a new custom field if I wanted to do this, should make it non-specific so that i can be reused -->
<!-- 	<h2>TITLE</h2>
	<p>PARA</p> -->

	<h2 class="pi-h2">PRODUCTS</h2>
	<p class="pi-p">We take great pride in producing products that feature both innovative technology and excellent quality.</p>


	<div class="main-grid grid-container grid-cust">
		<main class="grid-x grid-margin-x grid-xy-cust">
			<?php 
				// WP_Query arguments
				$args = array(
					'post_type'              => array( 'products' ),
					'posts_per_page'         => '-1',
					'order'                  => 'ASC',
					'orderby'                => 'menu_order',
					'post_parent'			 => 0
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();

						//me testing the query, getting the post id
						//$child = $post->ID;
						//echo $child;

						// do something
						get_template_part( 'template-parts/content-product'); 
					}
				} else {
					// no posts found
				}

				// Restore original Post Data
				wp_reset_postdata();
			?>
		</main>
	</div>
</div>



<?php get_footer();
