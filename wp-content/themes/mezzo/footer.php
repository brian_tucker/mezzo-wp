<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<footer class="footer-container">
	<div class="footer-grid">
		<!-- <?php dynamic_sidebar( 'footer-widgets' ); ?> -->
		<?php $foot_info = get_field('footer_info', 'options'); ?>
	
		<!-- The footer line break -->
		<hr>

		<!-- These are on the same row -->
		<div class="foot-head-box">
			<h5><?php echo $foot_info['header'] ?></h5>
			<a href=<?php echo $foot_info['button_link']['url']; ?>><button id="foot-butt1"><?php echo $foot_info['button']?></button></a>
		</div>
		

		<!-- The ones below are on the same row -->
		<div class="foot-info-box">
			<div>
				<p>
					<?php echo $foot_info['address_line_1']?>
				</p>
				<p>	
					<?php echo $foot_info['address_line_2']?>
				</p>
			</div>

			<div>
				<p class="foot-num-head">For Heat Exchangers</p>
				<p class="foot-num">
					<?php echo $foot_info['heat_number']?>
				</p>
			</div>

			<div>
				<p class="foot-num-head">For All Other Inquiries:</p>
				<p class="foot-num">
					<?php echo $foot_info['other_number']?>
				</p>
			</div>
		</div>

		<button id="foot-butt2"><?php echo $foot_info['button']?></button>
		<!-- The footer line break -->
		<hr id="hr1">
	</div>
	<div id="foot-legal">
		<p id="foot-copy">
			Copyright <span>&#169;</span> <span id="foot-year">20xx</span> Mezzo Technologies. All rights reserved.
		</p>
		<p><a id="a1" href=<?php echo $foot_info['privacy_policy_link']['url']; ?>>Privacy Policy</a> | <a id="a2" href="https://gatorworks.net/">Site by Gatorworks</a></p>  
	</div>
	<img class="responsive" id="foot-logo" src="/wp-content/themes/mezzo/dist/assets/images/footer/white.png">
	
</footer>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>

<?php wp_footer(); ?>


<!-- this is the browser sync stuff -->
<?php if (strpos(get_bloginfo('url'), ':88')): ?>
	<script id="__bs_script__">//<![CDATA[
		document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));
	//]]></script>
<?php endif; ?>

</body>
</html>
