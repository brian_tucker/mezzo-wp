<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php 
		$header_info = get_field('header_info', 'options');
		$phone = $header_info['phone'];
	?>
	<!-- This is the main container for the header, the parent -->
	<header class="site-header" role="banner">

		<!-- THIS IS THE MOBILE MENU -->
		<div class="site-title-bar mezzo-title-bar" id="mezzo-title-bar">

			<!-- THIS IS THE PHONE ICON -->
			<div class="title-bar-left">

				<!-- This is where the phone is gonna go -->
				<a href="tel:<?php echo $phone; ?>"><img class="mobile-phone-icon" src="/wp-content/themes/mezzo/dist/assets/images/mobile/phone.png"></a>
			</div>

			<!-- THIS IS THE LOGO IN THE MIDDLE -->
			<div class=title-bar-center >
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="/wp-content/themes/mezzo/dist/assets/images/mobile/logo.png">
				</a>
			</div>

			<!-- THIS IS THE SANDWICH MENU -->
			<div class="title-bar-right">
				<!-- This is where the menu bar is going to go -->

				<!-- This is the menu toggler -->
				<div class="toggle-container" data-toggle="offCanvas menu-toggler menu-text <?php foundationpress_mobile_menu_id(); ?>">
					<div id="menu-toggler" data-toggler=".open">
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					</div>
					<div id="menu-text" data-toggler=".open">
						<span class="menu-letters">menu</span>
						<span class="close-letters">close</span>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- THIS IS THE FULL SCREEN MENU -->
		<nav class="site-navigation top-bar mezzo-head" role="navigation" id="">
			<!-- this is where the logo is gonna go -->
			<div class="top-bar-left">
				<div class="site-desktop-title top-bar-title">
					<!-- <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> -->
					<a id="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"> 
						<img src="/wp-content/themes/mezzo/dist/assets/images/header/black.png"> 
					</a>
				</div>
			</div>
			
			<!-- This is where the menu options are -->
			<div class="top-bar-right">
				

				<div id="head-info">
					<p id="head-num-text">
						Call Now 
						<a href="tel:<?php echo $phone; ?>" class="phone-link track-phone-call-conversion"><strong><?php echo $phone; ?></strong></a>
					</p>
				</div>

				<div class="show-for-large mega-nav-cont">
					<div class="header-info-cont" id="header-info-cont">
						<!-- I'm gonna want to loop through the data here...how so --> 
						<?php 

							//these are the menu items from the primary nav....do we need these?
							$items  =  wp_get_nav_menu_items('primary-nav'); 


							//initializing the arrays to hold the parent and the children
							$parent_arr = array();
							$child_arr = array();


							foreach($items as $item)
							{	

								$menu_ID = $item->{'ID'};
								$menu_post_title = $item->{'post_title'};
								$menu_item_parent = $item->{'menu_item_parent'};
								$menu_title = $item->{'title'};

								//if the element doesn't have a parent, then it is the root
								if($menu_item_parent == 0)
								{
									//pushing the parent menu items to the array
									array_push($parent_arr, $item);
								}
								else {
									//pushing the children items into the array
									array_push($child_arr, $item);

									//if the parent is products, then do this
									if($menu_item_parent == 49 || $menu_item_parent == 47)
									{

										//getting the field for products
										$pi_content = get_field('pi_content', $item->{'object_id'});
										//getting the field for industries
										$industry_content = get_field('industries_content', $item->{'object_id'});
										if($menu_item_parent == 49)
										{
											//the image for the product or industry
											$main_image = $pi_content['main_image']['url'];
											// the title for the product or industry
											$title = $pi_content['title'];
											// the description for the product or industry
											$desc  = $pi_content['desc'];
										}
										else{
											//the image for the product or industry
											$main_image = $industry_content['main_image']['url'];
											$bg_image = $industry_content['bg_image']['url'];
											// the title for the product or industry
											$title = $industry_content['title'];
											// the description for the product or industry
											$desc  = $industry_content['desc'];
										}

										//displaying the card using the values we want.
										echo '<div class="pi-header-info-cont" id="c-'. $item->{'ID'} .'">';

										//checking to see what the parent is to determine what kind of element to return.
										if($menu_item_parent == 49)
										{
											echo '<div class="pi-header-img-cont" id="c-pi-img-'. $item->{'ID'} .'" >
													<img class="pi-header-info-img" src="'. $main_image .'">
												  </div>';
										}
										else
										{
											echo '<div class="pi-header-img-cont" id="c-ind-img-'. $item->{'ID'} .'" >';

											//checking to see if the src for the main image exists, if not, don't display this image.
											if($main_image != "" && !is_null($main_image))
											{
												echo '<img class="pi-header-info-img-ind" src="'. $main_image .'">';
											}

											echo	'<img class="pi-header-info-bg" src="'. $bg_image .'">
												  </div>';
										}

												echo '<div class="pi-header-text-cont" id="c-text-'. $item->{'ID'} .'">';
												echo '<h4>' . $title . '</h4>';
												echo '<p>' . $desc . '</p>';
											echo '</div>';
										echo '</div>';
									}
								}
							}	
						?>
					</div>
					<?php foundationpress_top_bar_r(); ?>	
				</div>
				<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
					<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
				<?php endif; ?>
			</div>
		</nav>

	</header>
	<div class="off-canvas-wrapper">
		<div class="off-canvas position-right mezzo-menu" id="offCanvas" data-off-canvas>
			<?php foundationpress_mobile_nav() ?>
		</div>
	</div>