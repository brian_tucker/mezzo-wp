<div class="why-faqs-cont">
	<!-- displaying the questions by looping through them. That way it'll grow if it needs to -->
	<?php

		//getting the faqs and the faq answers. There needs to be the same amount...might be able to be improved into an object array?
		// $faqs = get_field('faqs');
		// $faqs_a = get_field('faqs_answers');

		$faqs = get_field('faqs');

		if($faqs)
		{
			//initializing the ul with the foundation classes
			echo '<ul class="vertical menu accordion-menu why-accordion" data-accordion-menu>';

			foreach($faqs as $faq) {
				echo '<li> 
						<a class="main_a">' . $faq['question'] . '
						<div class="circle-cont">
			 				<div class="circle" id="circle">
			 				</div>
			 			</div></a>
						
						<ul class="menu vertical nested">
							<li><a href="#">'. $faq['answer'] . 
						
					 			'</a>
					 		</li> 
					 	</ul>	
					 </li>';
			}
			echo '</ul>';
		}
	?>
</div>