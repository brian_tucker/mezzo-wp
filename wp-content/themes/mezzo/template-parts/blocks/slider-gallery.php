<div class="slider-cont">
	<?php
		$images = get_field('slider_gallery');

		if( have_rows('slider_gallery') ) :
			echo '<div class="slider-for">';
				while ( have_rows('slider_gallery') ) : the_row();

				$image = get_sub_field('image');
				?>

					<div><img src="<?php echo $image['url']; ?>"/></div>
				<?php
				endwhile;
			echo '</div>
					<div class="slider-nav">';

				while(have_rows('slider_gallery') ) : the_row();

				$image = get_sub_field('image');	

				?>
					<div><img src="<?php echo $image['url']; ?>"/></div>
				<?php 
				endwhile;
			echo '</div>';
		endif;
	?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.slider-for').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: false,
			  fade: true,
			  asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
			  slidesToShow: 3,
			  slidesToScroll: 1,
			  asNavFor: '.slider-for',
			  dots: false,
			  centerMode: false,
			  focusOnSelect: true
			});
		});

	</script>
</div>