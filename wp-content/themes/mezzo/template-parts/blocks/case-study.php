<!-- defining the cases var -->
<?php $cases = get_field('case_study');?>



<ul class="case-list">

	<?php foreach($cases as $case) { ?>
		<li>
			<div class="case-study-gradient"></div>

			<div class="case-study-cont">
				<img class="float-image" src="<?php echo $case['main_image']['url']?>">
				<div class="case-header-cont">
					<h4><?php echo $case['title']; ?></h4>
					<h2><?php echo $case['header'] ?></h2>
				</div>

				<div><?php echo $case['paragraph']; ?></div>
			</div>
		</li>
	<?php } ?>
</ul>