<div class="cta-cont">
	<!-- <h1>This is the custom CTA</h1> -->
	<?php $cta = get_field('content'); ?>	
	<?php $push = $cta['push_main_image'] ?>

	<div class="text-cont">
		<h1><?php echo $cta['header']?></h1>
		<p><?php echo $cta['paragraph']?></p>
		<a href=<?php echo $cta['button_link']['url']; ?>><button><?php echo $cta['button']; ?></button></a>
	</div>

	<div class="left-div"></div>
	<div class="left-div2"></div>
	<!-- <div class="right-div"></div> -->

	<!-- Defining the images and displaying them below -->
	<?php $image1 = $cta['image_1'] ?>
	<?php $image2 = $cta['image_2'] ?>
	<?php $image3 = $cta['image_3'] ?>
	<?php $image4 = $cta['image_4'] ?>
	

	


	<img id="yellow-rect-mobile1" src="/wp-content/themes/mezzo/dist/assets/images/why/yellow-rect-mobile1.png">
	<img id="yellow-rect-mobile2" src="/wp-content/themes/mezzo/dist/assets/images/why/yellow-rect-mobile2.png">
	
	<img id="yellow-rect" src="/wp-content/themes/mezzo/dist/assets/images/why/yellow-rect.png">
	<img id="blue-rect" src="/wp-content/themes/mezzo/dist/assets/images/why/blue-rect.png">
	<?php if( $push == "1") {?>
		<img class="push-img-down" id="main-img" src="<?php echo esc_url($image1['url']) ?>">
	<?php } else { ?>
	
		<img  id="main-img" src="<?php echo esc_url($image1['url']) ?>">
	<?php } ?>
	
	<img id="cta-bg" src="<?php echo esc_url($image2['url']) ?>">
	<img id="cta-bg-2" src="<?php echo esc_url($image3['url']) ?>">
</div>