<div class="grid-container emp-main">
	<div class="grid-x grid-margin-x">

		<?php 

			// the_field('employee_card');
			//defining the repeater
			$cards = get_field('employee_card');

			foreach($cards as $card)  {

				echo '<div class="employee-card-cont cell medium-12 large-4">';

					echo '<div class="emp-img-cont"><img class="emp-img" src="'. $card['employee_image']['url'] .'" ></div>';

					echo '<div class="emp-text-cont">';
						echo '<h2>' . $card['name_header'] . '</h2>';

						echo '<h4>' . $card['role_header'] . '</h4>' ; 

						echo '<p class="emp-desc-full">' . $card['description'] . '</p>';

						echo '<div class="circle-cont circle-hide"><p class="circle-text">About</p><div class="circle emp-desc-circle"><p class="circle-plus">+</p></div></div>';

						echo '<div class="circle-cont2 circle-hide"><div class="circle"><p class="circle-plus">-</p></div></div>';

					echo '</div>';

					echo '<p class="emp-desc">' . $card['description'] . '</p>';

					echo '<img class="emp-mask" src="/wp-content/themes/mezzo/dist/assets/images/capabilities/mask.png">';
				echo '</div>';
			}

		?>
	</div>
</div>