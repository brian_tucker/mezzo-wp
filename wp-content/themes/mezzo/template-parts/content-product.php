<!-- displaying the cards that I find -->
<?php 
		//getting the content from the global custom field
		$pi_content = get_field('pi_content');
?>
<div class="pi-card">
	<div class="mobile-gradient"></div>
	<a href="<?php the_permalink(); ?>">	
			<img class="pi-card-img" id="pi-blue-rect" src="/wp-content/themes/mezzo/dist/assets/images/products_industries/rect_1.png" > 
			<div class="yellow-rect-cont">
				<div class="yellow-rect-text-cont">
					<p class="yellow-rect-plus">+</p>
					<p class="yellow-rect-text">Read More</p>
					<img id="pi-card-arrow" src="/wp-content/themes/mezzo/dist/assets/images/products_industries/pi-card-arrow.png">
				</div>
				<img class="pi-card-img yellow-rect" id="pi-yellow-rect" src="/wp-content/themes/mezzo/dist/assets/images/products_industries/rect_2.png" >
				<img class="pi-card-img yellow-rect-ext" id="pi-yellow-rect-ext" src="/wp-content/themes/mezzo/dist/assets/images/products_industries/rect_2_ext.png" >
			</div>
			
			<img class="pi-card-img" id="pi-main-img" src="<?php echo $pi_content['main_image']['url'] ?>" >
			<img class="pi-card-img" id="pi-bg" src="<?php echo $pi_content['bg_image']['url'] ?>" >
		
		<div class="pi-card-text-cont">
			<h2 class="pi-card-header" id="" ><?php the_title() ?></h2>
			<!-- the stuff below is used to display the children pages -->
			<?php 
				$args = array(
				    'post_type'      => 'products',
				    'posts_per_page' => -1,
				    'post_parent'    => $post->ID,
				    'order'          => 'ASC',
				    'orderby'        => 'menu_order'
				 );

				$children_query = new WP_Query( $args );

				if( $children_query -> have_posts() ) :
				 while ( $children_query->have_posts() ) : $children_query->the_post(); ?>

				        <div id="parent-<?php the_ID(); ?>" class="parent-page">
							<!-- this line are the children but clickable. Not sure if these should be clickable. -->
				            <p class="pi-card-text" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></p>
				        </div>

				    <?php endwhile; ?>

				<?php endif; wp_reset_postdata(); ?> 
		</div>
	</a>
</div>


