<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<article class="cell medium-12 large-4" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content news-cont ">
		<!-- <?php get_template_part( 'template-parts/featured-image' ); ?> -->
		
		<!-- getting the featured image -->
		<div class="thumb-cont">
			<?php 
				if ( has_post_thumbnail() ) {
				    the_post_thumbnail();
				}
			?>
		</div>
		


		<!-- getting the title -->
		<h4><?php the_title(); ?></h4>

		





		<!-- the button to read more -->
		<button><a href="<?php the_permalink(); ?>"> Read More</a></button>
	</div>
</article>
